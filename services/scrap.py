import os
import requests
from bs4 import BeautifulSoup
import pandas as pd

# data with the final result data
final_data = []

# create csv file if it does not exist
def create_csv_file(path, filename):
    if(not os.path.exists(path + "/" + filename)):
        os.system("touch doctor_list.csv")
    


# extract 20 first doctors data from the first pagination
def extract_doctor_data() -> list:
    URL = "http://annuairesante.ameli.fr/recherche.html"
    headers = {'User-Agent' : "Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0"}

    payload = {
        "type": "ps",
        "ps_nom": "",
        "ps_profession": "34",
        "ps_profession_label": "Médecin+généraliste",
        "ps_acte": "",
        "ps_acte_label": "",
        "ps_type_honoraire": "indifferent",
        "ps_carte_vitale": "2",
        "ps_sexe": "2",
        "es_nom": "",
        "es_specialite": "",
        "es_specialite_label": "",
        "es_actes_maladies": "",
        "es_actes_maladies_label": "",
        "es_type": "3",
        "ps_localisation": "HERAULT+(34)",
        "ps_proximite": "on",
        "localisation_category": "departements",
        "submit_final": "Rechercher"
    }

    res = requests.post(URL, headers=headers, params=payload)
    soup = BeautifulSoup(res.content, "html.parser")

    doctors = soup.find_all("div", class_="item-professionnel")

    data = []

    for doctor in doctors:
        info = {}
        info["name_firstname"] = doctor.h2.a.text
        
        phone = doctor.find("div", class_="item left tel")
        if(phone):
            info["phone"] = doctor.find("div", class_="item left tel").text      
        else:
            info["phone"] = None

        info["address"] = doctor.find("div", class_="item left adresse").text

        data.append(info)
    
    return data

# check existing data and add new data
def add_new_data(data):
    for element in data:
        if element not in final_data:
            final_data.append(element)


# loop to add new data until, it got all 1000 data
def collect():
    while len(final_data) < 1000:
        data = extract_doctor_data()
        add_new_data(data)
        print("Nombre de nouvelles informations acquis : ", len(final_data))

# write in csv file all the data
def persist():
    df = pd.DataFrame(final_data)
    df.to_csv('/home/mongodb-scraping/models/csv/doctor_list.csv', index=False)
    return

def get_scrap_result():
    return final_data

collect()
persist()